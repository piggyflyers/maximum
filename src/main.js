import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import filter from 'vue-bulma-emoji/src/filter'
import '@/tools/Locals.js'

Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper)
Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube' })
Vue.use(filter)
Vue.use(filter)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
