import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Exam from './views/Exam.vue'
import Contacts from './views/Contacts.vue'
import Error404 from './views/Error404.vue'
import Error503 from './views/Error503.vue'
import Events from './views/Events.vue'
import EventOnline from './views/EventOnline.vue'
import EventOffline from './views/EventOffline.vue'
import Methods from './views/Methods.vue'
import Teachers from './views/Teachers.vue'
import Platform from './views/Platform.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/exam',
      name: 'exam',
      component: Exam
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: Contacts
    },
    {
      path: '/404',
      name: 'Error404',
      component: Error404
    },
    {
      path: '/503',
      name: 'Error503',
      component: Error503
    },
    {
      path: '/events',
      name: 'Events',
      component: Events
    },
    {
      path: '/event-online',
      name: 'EventOnline',
      component: EventOnline
    },
    {
      path: '/event-offline',
      name: 'EventOffline',
      component: EventOffline
    },
    {
      path: '/methods',
      name: 'Methods',
      component: Methods
    },
    {
      path: '/teachers',
      name: 'Teachers',
      component: Teachers
    },
    {
      path: '/platform',
      name: 'Platform',
      component: Platform
    }
  ]
})
