import Vue from 'vue'

Vue.filter('date', function (value) {
  if (!value) return ''
  const day = value.getDate()
  const month = value.getMonth()
  const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']
  return `${day} ${months[month]}`
})

Vue.filter('week-day', function (value) {
  if (!value) return ''
  const day = value.getDay()
  const days = ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
  return `${days[day]}`
})

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('status', function (value) {
  if (!value) return ''
  if (value === 'онлайн') {
    return 'online'
  } else if (value === 'оффлайн') {
    return 'offline'
  }
})
